import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}

class ScreenArguments{
  final String title;
  final String message;

  ScreenArguments(this.title, this.message);
  
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    routes: {
      ExtractArgumentsScreen.routeName: (context) => const ExtractArgumentsScreen(),
    },
    home: HomeScreen(),
  );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pushNamed(context, ExtractArgumentsScreen.routeName, 
            arguments: ScreenArguments('Extract Arguments', 'Test Test Message 1 2 3'));
          },
          child: Text('Navigate to extract screen'),
        ),
      ),
    );
  }
}

class ExtractArgumentsScreen extends StatelessWidget {
  const ExtractArgumentsScreen({Key? key}) : super(key: key);
  static const routeName = '/extractArguments';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      body: Center(child: Text(args.message),),
    );
  }
}